"use strict";
let chai = require('chai');
let chaiHttp = require('chai-http');
chai.use(chaiHttp);

let test_server = "http://dummy.restapiexample.com/api/v1";
const fixture = require('../resources/fixture');

function getFixtures (callback) {
  chai.request(test_server)
    .get('/employees')
    .end(function(err, res){
      if (err)
        console.error(err);
      callback(err, res);
  });
};

exports.getFixtures = getFixtures;
